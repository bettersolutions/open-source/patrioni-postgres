FROM registry.opensource.zalan.do/acid/spilo-10:1.4-p7
COPY config/configure_spilo.py /configure_spilo.py
CMD ["/bin/sh", "/launch.sh"]
